package fr.esiee.example.pic;

import com.github.lalyos.jfiglet.FigletFont;

// bonjour cest etienne
// kikoo c'est alexis et said
/**
 * affiche le mot en parametre en ascii
 */
public class WordsPrinter {
	public final static void main(String[] args) {
		String valueToPrint = "rosith sammy antoine cuyny claire Matzinger et Ahmed dkhissi et timothee et etienne et alexis et said et QuentinLamotteEtAdrienBrugirard  TRACY NGOT | HAIFENG TAN et Nicolas LU et David Picaud";

		String valueToPrintInAsciiArt = FigletFont.convertOneLine(valueToPrint);
		String TP = "Cunyang GONG & Jie YU";
		// Affichage avec ascii art
		System.out.println(valueToPrintInAsciiArt);
		System.out.println(TP);
	}
}
